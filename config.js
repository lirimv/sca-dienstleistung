module.exports = {
    siteUrl: '',
    siteTitle: 'Sca-Dienstleistung', // <title>
    siteLanguage: 'de',
    manifestName: 'Sca-Dienstleistung',
    manifestShortName: 'CMS', // max 12 characters
    manifestStartUrl: '/',
    manifestBackgroundColor: '#663399',
    manifestThemeColor: '#663399',
    manifestDisplay: 'standalone',
    manifestIcon: '',
    pathPrefix: '', // This path is subpath of your hosting https://domain/portfolio
    keywords: 'werbung, direktwerbung, fyler verteilen',
    
  
  };
  