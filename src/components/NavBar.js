import React from "react";
import "./layout.css";
import { slide as Menu } from "react-burger-menu";

export default props => {
  return (
    // Pass on our props
    <Menu {...props}>
      <a className="menu-item" href="/">
        Home
      </a>

      <a className="menu-item" href="./impressum">
        Impressum
      </a>

      <a className="menu-item" href="./datenschutzerklarung">
       Datenschutzerklarung
      </a>
    </Menu>
  );
};




