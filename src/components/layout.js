import React from "react"
import "./layout.css"
import Image from './image'
import Footer from './footer'
import { ArenguForm } from "gatsby-plugin-arengu-forms"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faBroom, faDumpster, faHome } from '@fortawesome/free-solid-svg-icons'
import Navbars from "./NavBar"


const Layout = () => {
  

  return (
    <>
     <Navbars pageWrapId={"page-wrap"} outerContainerId={"App"}/>
     <section className="banner">
      <div className="about-us">
      <h1>SCA-Dienstleistung</h1>
        <h2>Willkommen</h2>
        <h2>SEITE IN ÜBERARBEITUNG!</h2>
        <h2>- Sercan Camuka -</h2>
        <div className="about-p">
        <h3>– Ihr zuverlässiger Partner für Abbruch und Asbest</h3>
        <p>Sehr geehrte Damen und Herren,hiermit möchten wir uns als ein starker Partner im Bereich Abbruch und Asbestsanierung bei Ihnen vorstellen. 
          Sie können von unseren kompetenten Mitarbeitern, die über langjährige Erfahrung in Abbruch,
           Entrümpelung, Entkernung und Sanierung verfügen, profitieren.
           Wir arbeiten bei der Asbestsanierung nach TRGS 519, zur Zeit allerdings nur mit festgebundenen Produkten.</p>
        </div>
        <div className="button-more">
         <a href="#services" className="more">MEHR ERFAHREN</a>
        </div>
      </div>
     </section>
     <section  id="services"className="services">
       <h1>Unsere Leistungen umfassen im Einzelnen</h1><br/>
       <div className="icon-services">
       <p>Profitieren Sie von unserem unermüdlichen Einsatz für unsere Kunden und kontaktieren Sie uns gerne jederzeit für ein individuelles Angebot.</p>
       <ul className="icons major">
          <li>
            <span className="icon major style1">
            <FontAwesomeIcon icon={faDumpster} />
            </span>
            <p>Asbest-Sanierung und gesetzeskonforme Entsorgung</p>
          </li>
          <li>
            <span className="icon major style1">
            <FontAwesomeIcon icon={faBroom} />
            </span>
            <p>Entkernung und Entrümpelung von Räumen und Gebäuden</p>
          </li>
          <li>
            <span className="icon major style1">
            <FontAwesomeIcon icon={faHome} />
            </span>
            <p>Fachgerechter Umbau von Asbest-belasteten Gebäudeteilen</p>
          </li>
        </ul>
       </div>
     </section>
     <section>
       <div className="gallery">
         <Image/>
       </div>
     </section>
     <section className="contanct-form">
         <div className="contanct">
           <h1>Kontaktieren Sie uns</h1>
           <div className="form">
           <ArenguForm  className="form" id="160000066796213440" />
           </div>
         </div>
     </section>
     <section>
       <div>
         <Footer/>
       </div>
     </section>
    </>
  )
}



export default Layout
