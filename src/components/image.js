import React from 'react'
import ImageGallery from 'react-image-gallery';

 
const images = [
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/7c05245c-5588-434a-a659-77f3a55ff42b.JPG',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/7c05245c-5588-434a-a659-77f3a55ff42b.JPG',
  },
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/ef23bcba-2843-4dbc-8f78-5eca6afa24c6.png',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/ef23bcba-2843-4dbc-8f78-5eca6afa24c6.png',
  },
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/e4c4c033-a86b-4060-a9aa-043e7a3a40d0.JPG',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/e4c4c033-a86b-4060-a9aa-043e7a3a40d0.JPG',
  },
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/c8285905-bc88-47a4-8f36-4f8fd0d329d4.JPG',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/c8285905-bc88-47a4-8f36-4f8fd0d329d4.JPG',
  },
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/319b14b7-600d-4d16-9078-71c4e582724a.JPG',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/319b14b7-600d-4d16-9078-71c4e582724a.JPG',
  },
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/60917b1e-0919-4d13-9155-c0eaf2afd890.JPG',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/60917b1e-0919-4d13-9155-c0eaf2afd890.JPG',
  },
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/c6c1fb1c-5f31-404b-b027-988e53f9b3ac.JPG',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/c6c1fb1c-5f31-404b-b027-988e53f9b3ac.JPG',
  },
  {
    original: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/610c495c-6f52-4a12-b313-0b9f6b8b5bbb.JPG',
    thumbnail: 'https://sca-dienstleistung.de/wp-content/gallery/bilder-von-baustellen/610c495c-6f52-4a12-b313-0b9f6b8b5bbb.JPG',
  },
];
 
class Image extends React.Component {
  render() {
    return <ImageGallery items={images} />;
  }
}

export default Image
